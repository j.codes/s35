const express = require("express")
const mongoose = require("mongoose")
const dotenv = require("dotenv")

dotenv.config()

const app = express();
const port = 3001

// MongoDB Connection
mongoose.connect(`mongodb+srv://jcodesx:${process.env.MONGODB_PASSWORD}@cluster0.hiowweg.mongodb.net/?retryWrites=true&w=majority`, {
	useNewUrlParser: true,
	useUnifiedTopology: true
})

let db = mongoose.connection

db.on('error', () => console.log("Connection error."))
db.on('open', () => console.log("Connected to MongoDB!"))
// MongoDB Connection END

app.use(express.json())
app.use(express.urlencoded({extended: true}))

// MongoDB Schemas
const userSchema = new mongoose.Schema({
	username: String,
	password: String
})
// MongoDB Schemas END

// MongDB Model
const User = mongoose.model('User', userSchema)
// MongDB Model END

// Routes
app.post('/signup', (request, response) => {
	User.findOne({username: request.body.username}, (error, result) => {
		if (result != null && result.username == request.body.username){ 
			return response.send('Duplicate user found!') 
		}

		let newUser = new User({
			username: request.body.username,
			password: request.body.password
		})

		newUser.save((error, savedUser) => {
			if(error){
				return console.error(error)
			} else {
				return response.status(200).send('New user registered!')
			}
		})
	})
})
// Routes END

app.listen(port, () => console.log(`Server running at localhost:${port}`))